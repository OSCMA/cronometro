from Hora import *
from Minuto import *
from Segundo import *
from Decima import *

class Cronometro():
    def __init__(self):
        self.h = Hora()
        self.m = Minuto()
        self.s = Segundo()
        self.d = Decima()


    def avanzar(self):
        self.d.avanzar()
        if self.d.getValor() == 0:
            self.s.avanzar()
            if self.s.getValor() == 0:
                self.m.avanzar()
                if self.m.getValor() == 0:
                    self.h.avanzar()


    def retroceder(self):
        self.d.retroceder()
        if self.d.getValor() == self.d.getTope():
            self.s.retroceder()
            if self.s.getValor() == self.s.getTope():
                self.m.retroceder()
                if self.m.getValor() == self.m.getTope():
                    self.h.retroceder()


    def getTiempo():
        hora=["00:02:03:10"]
        hora2=[h.split(":") for h in horas]
        return hora2
        
        
        


    def getTiempo(self):
        return str(self.h.getValor())+":"+str(self.m.getValor())+":"+str(self.s.getValor())+":"+str(self.d.getValor())
    
